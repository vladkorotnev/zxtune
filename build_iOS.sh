#!/usr/bin/env bash

# General paths
CPUS=4
FINAL_NAME="libzxtune.dylib"
TARGET="apps/libzxtune"
OFXBOOST_PATH="/Users/virtualmachine/Downloads/ofxiOSBoost-master"

# iOS specific settings
# ! Warning:
IOS_ARCHS="armv7s arm64"
IOS_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS.sdk"
# Simulator specific settings
SIM_ARCHS="x86_64"
SIM_SDK="/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator.sdk"

set -e

if [ ! -e "$IOS_SDK" ];
then
    echo "iOS SDK was not found at: $IOS_SDK"
    exit 1
fi

if [ ! -e "$SIM_SDK" ];
then
    echo "iOS Simulator SDK was not found at: $SIM_SDK"
exit 1
fi

if [ ! -e "$OFXBOOST_PATH" ];
then
    echo "ofxiOSBoost repository not found at: $OFXBOOST_PATH"
    echo "Please download from https://github.com/danoli3/ofxiOSBoost"
exit 1
fi

do_build() {
    make release=1 -C $TARGET clean
    make release=1 -C $TARGET -j $CPUS
}

make_vars() {
cat >"variables.mak" <<EOVARS
toolchains.root=/usr/bin
cpus=$CPUS
platform=darwin
arch=$ANAME
link=static
prebuilt.dir=$OFXBOOST_PATH
darwin.cxx.flags += -arch $ARCHS -miphoneos-version-min=9.0 -isysroot $SDK -Wno-error -Wno-implicit-function-declaration
darwin.ld.flags += -arch $ARCHS -miphoneos-version-min=9.0 -isysroot $SDK -install_name "@rpath/$FINAL_NAME"
EOVARS
}

echo "libzxtune iOS build-script by akasaka 2018"

mkdir -p _iphoneos

echo "--------------------------------------------------------"
echo "Building for iOS ARM"

for arch in $IOS_ARCHS
do
SDK="$IOS_SDK"
ARCHS="$arch"
ANAME="arm"
make_vars
do_build
# move product for later lipo-fication
mv "bin/darwin_$ANAME/release/libzxtune.dylib" "_iphoneos/libzxtune_$arch.dylib"
done
echo "--------------------------------------------------------"


echo "Building for iOS Simulator"

SDK="$SIM_SDK"
ANAME="x86_64"
ARCHS="$SIM_ARCHS"
make_vars
do_build
mv "bin/darwin_$ANAME/release/libzxtune.dylib" "_iphoneos/libzxtune_simulator.dylib"

echo "Merging to universal library with lipo"

cd _iphoneos
lipo -create *.dylib -output "$FINAL_NAME"

echo "Copying headers..."
cp ../apps/libzxtune/zxtune.h ./

echo "+ Done."
open _iphoneos
